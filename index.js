const WyreClient = require("@wyre/api").WyreClient

/*
 * Root account credentials
 * TODO: get from Jack's testwyre account
 * NOTE: you will have to create new API Key to get the Secret Key because it will be deleted immediately
 */
const API_KEY = "",
    SEC_KEY = "",
    rootAccountId = ""

const wyre = new WyreClient({
    format: "json_numberstring",
    apiKey: API_KEY,
    secretKey: SEC_KEY,
    baseUrl: "https://api.testwyre.com"
})

// Get root account info
wyre.get(`/v3/accounts/${rootAccountId}`)
    .then(acc => console.log(acc))
    .catch(err => console.log(err))

// Create a child account
// The child will be associate to root derived from the credentials supplied in WyreClient's constructor
wyre.post("/v3/accounts", {
    type: "INDIVIDUAL",
    country: "US",
    subaccount: true,
    profileFields: [
        {
            fieldId: "individualLegalName",
            value: "John Doe"
        },
        {
            fieldId: "individualEmail",
            value: "johndoe@gmail.com"
        },
        {
            fieldId: "individualCellphoneNumber",
            value: "+11234567890" // remember +1 for US
        }
    ]
})
    .then(res => console.log(res))
    .catch(err => console.log(err))

/* TODO: replace with the account id from above creation

const childAccountId = ""

// To make any API request to child account, we have to masquerade as the child
// We can ONLY masquerade to our children, which makes perfect sense for security
let child = wyre.masqueraded(childAccountId)
child
    .get(`/v3/accounts/${childAccountId}`)
    .then(acc => console.log(acc))
    .catch(err => console.log(err))

*/

// Make a transfer
wyre.post("/v2/transfers", {
    sourceCurrency: "DAI",
    dest: "account:" + childAccountId,
    destAmount: 1,
    destCurrency: "DAI",
    message: "Here's some DAI!",
    autoConfirm: true // Wyre does not recommend doing this, but only for demonstration, so meh
})
    .then(res => console.log(res))
    .catch(err => console.log(err))
